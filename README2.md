This math is inline $`a^2+b^2=c^2`$.

This is on a separate line

```math
a^2+b^2=c^2
```

```math
\begin{Bmatrix}
   a & b \\
   c & d
\end{Bmatrix}
```